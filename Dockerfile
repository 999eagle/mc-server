FROM openjdk:8-jre

ENV FORGE_BASE_VERSION=10.13.4.1614 \
	MINECRAFT_VERSION=1.7.10
ENV FORGE_VERSION=${MINECRAFT_VERSION}-${FORGE_BASE_VERSION}-${MINECRAFT_VERSION}

ENV MINECRAFT_JAR=minecraft_server.${MINECRAFT_VERSION}.jar \
	FORGE_JAR=forge-${FORGE_VERSION}-universal.jar \
	FORGE_INSTALLER=forge_installer.jar

RUN mkdir -p /minecraft/server && \
	cd /minecraft && \
	wget -O ${MINECRAFT_JAR} "https://s3.amazonaws.com/Minecraft.Download/versions/${MINECRAFT_VERSION}/minecraft_server.${MINECRAFT_VERSION}.jar" && \
	wget -O ${FORGE_INSTALLER} "https://files.minecraftforge.net/maven/net/minecraftforge/forge/${FORGE_VERSION}/forge-${FORGE_VERSION}-installer.jar" && \
	java -jar ${FORGE_INSTALLER} --installServer && \
	rm forge_installer.jar forge_installer.jar.log

COPY /build/run.sh /minecraft/

WORKDIR /minecraft/server
ENTRYPOINT ["/minecraft/run.sh"]
